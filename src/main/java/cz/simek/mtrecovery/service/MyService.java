package cz.simek.mtrecovery.service;

import java.util.Random;
import java.util.Set;

import cz.simek.mtrecovery.exception.NotActiveException;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class MyService extends Thread{

    private boolean isActive = false;

    @Override
    public void run() {
        isActive = true;
        log.info("MyService is running");
        try {
            Thread.sleep(60000);
            isActive = false;
            Thread.sleep(60000);
            isActive = true;
            Thread.sleep(60000);
            Set<Thread> threads = Thread.getAllStackTraces().keySet();
            threads.stream().filter(t -> t.getThreadGroup().getName().contains("MyThreads"))
                    .filter(t-> t.getState() == State.RUNNABLE || t.getState() == State.TIMED_WAITING )
                    .forEach(t -> t.interrupt());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    public int getRandomSleepTime(int min, int max) {
        if (isActive) {
            Random random = new Random();
            return random.ints(min, max).findFirst().getAsInt();
        }
        throw new NotActiveException("MyService is not active!!!");
    }

    public boolean isActive() {
        if (isActive) {
            return true;
        }
        throw new NotActiveException("MyService is not active!!!");
    }
}
