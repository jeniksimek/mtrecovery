package cz.simek.mtrecovery;

import cz.simek.mtrecovery.runnable.MyThread;
import cz.simek.mtrecovery.service.MyService;
import lombok.extern.slf4j.Slf4j;

import java.util.Random;

@Slf4j
public class MultiThreadRecovery {

    private static int MINIMAL_NUMBER_THREADS = 5;
    private static int MAXIMAL_NUMBER_THREADS = 10;

    public static void main(String[] args) {
        log.info("Hello, main thread of the application was started.");
        MyService myServiceThread = new MyService();
        myServiceThread.start();
        log.info("MyService started...");

        int numberOfTasks = getRandomNumberUsingInts(MINIMAL_NUMBER_THREADS, MAXIMAL_NUMBER_THREADS);
        log.info(String.format("The application starts %s threads.", numberOfTasks));

        ThreadGroup tg = new ThreadGroup("MyThreads");
        for (int i = 0; i < numberOfTasks; i++) {
            Thread thread = new Thread(tg, new MyThread("MyThread" + i, myServiceThread));
            thread.start();
        }
    }

    private static int getRandomNumberUsingInts(int min, int max) {
        Random random = new Random();
        return random.ints(min, max).findFirst().getAsInt();
    }
}
