package cz.simek.mtrecovery.runnable;

import cz.simek.mtrecovery.exception.NotActiveException;
import cz.simek.mtrecovery.service.MyService;
import lombok.extern.slf4j.Slf4j;

import java.text.MessageFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
public class MyThread implements Runnable {

    private int iteration = 0;
    private final int MINIMAL_SLEEP_TIME_IN_SEC = 2;
    private final int MAXIMAL_SLEEP_TIME_IN_SEC = 5;
    private int lastPrintBefore = 0;

    private boolean callHealthCheckFlag = false;
    private boolean terminateMyThread = false;

    private final String nameThread;
    private final MyService myService;

    public MyThread(String name, MyService myServiceThread) {
        nameThread = name;
        myService = myServiceThread;
    }

    @Override
    public void run() {
        synchronized (myService) {
            while (!terminateMyThread) {
                try {
                    if (callHealthCheckFlag) {
                        callHealthCheckFlag = isMyServiceAlive();
                        myService.notifyAll();
                    }
                    int randomSleepTimeInSec = myService.getRandomSleepTime(MINIMAL_SLEEP_TIME_IN_SEC, MAXIMAL_SLEEP_TIME_IN_SEC);
                    printThreadInformationAndWait(randomSleepTimeInSec);
                } catch (NotActiveException nae) {
                    List<Thread> runnableThreads = getRunnableThreadsInThreadGroup();
                    if (runnableThreads.size() > 1) {
                        pauseThread();
                    } else {
                        callHealthCheckFlag = true;
                        pauseThreadForFiveSeconds();
                    }
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                    return;
                }
            }
        }
    }

    private void printThreadInformationAndWait(int randomSleepTimeInSec) throws InterruptedException {
        iteration++;
        log.info(MessageFormat.format("{0} {1} Number of iteration: {2}. Last print was before: {3,number,#.##} s", nameThread, getNow(), iteration, lastPrintBefore));
        lastPrintBefore = randomSleepTimeInSec;
        myService.wait(randomSleepTimeInSec * 1000);
    }

    private List<Thread> getRunnableThreadsInThreadGroup() {
        String threadGroup = Thread.currentThread().getThreadGroup().getName();
        Set<Thread> threadSet = Thread.getAllStackTraces().keySet();
        List<Thread> runnableThreads = threadSet.stream()
                .filter(t -> t.getThreadGroup().getName().contains(threadGroup))
                .filter(t -> t.getState() == Thread.State.RUNNABLE || t.getState() == Thread.State.TIMED_WAITING)
                .collect(Collectors.toList());
        return runnableThreads;
    }

    private void pauseThread() {
        try {
            log.info(MessageFormat.format("{0} is waiting.", nameThread));
            myService.wait();
        } catch (InterruptedException e) {
            e.printStackTrace();
            Thread.currentThread().interrupt();
        }
    }

    private void pauseThreadForFiveSeconds() {
        try {
            log.info(MessageFormat.format("Name of active thread: {0}. Waiting for 5 second to call healthCheck.", nameThread));
            myService.wait(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
            Thread.currentThread().interrupt();
        }
    }

    private boolean isMyServiceAlive() {
        log.info("Healthcheck called...");
        boolean active = myService.isActive();
        return !active;
    }

    private String getNow() {
        return LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd/MM/YYYY"));
    }
}
